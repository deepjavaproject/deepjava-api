import matplotlib
import time
import cv2
import numpy as np
from keras.constraints import maxnorm
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Dropout, Flatten, Dense
from keras.optimizers import SGD
from keras.preprocessing.image import ImageDataGenerator
from PIL import Image
from base64 import decodestring
import os
import io
import tensorflow as tf
import base64

## Flask
from flask import Flask, render_template, request, Response
from flask_uploads import UploadSet, configure_uploads, IMAGES
from flask_cors import CORS

import jsonpickle

# Initialize the Flask application
app = Flask(__name__)
CORS(app)
print("Initialize Flask ...")

def buat_model(ep_model):
    epochs = ep_model
    lrate = 0.01
    decay = lrate / epochs
    # sgd = SGD(lr=lrate, momentum=0.9, decay=decay, nesterov=False)
    model_candi = Sequential()

    model_candi.add(Conv2D(32, (3, 3), input_shape=(32, 32, 1), activation='relu', padding='same'))
    model_candi.add(Dropout(0.2))
    model_candi.add(Conv2D(32, (3, 3), activation='relu', padding='same'))
    model_candi.add(MaxPooling2D(pool_size=(2, 2)))
    model_candi.add(Flatten())
    model_candi.add(Dropout(0.2))
    model_candi.add(Dense(500, activation='relu', kernel_constraint=maxnorm(3)))
    model_candi.add(Dropout(0.2))
    model_candi.add(Dense(200, activation='relu', kernel_constraint=maxnorm(3)))
    model_candi.add(Dropout(0.2))
    model_candi.add(Dense(20, activation='softmax'))

    model_candi.compile(loss='categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])

    return model_candi

#Initialize Model
batch_size = 32
classout = ['Ba','Ca','Da','Dha','Ga','Ha','Ja','Ka','La','Ma','Na','Nga','Nya','Pa','Ra','Sa','Ta','Tha','Wa','Ya']
model = buat_model(50)
model.load_weights('bobot_train_jawa.h5')
graph = tf.get_default_graph()

# route http to this method
@app.route('/', methods=['GET'])
def index():
    # return render_template('home.html')
    return """
            <h1>DeepJava 2018.</h1>
            <p>It is currently {time}.</p>
            <img src="http://loremflickr.com/600/400" />
            """.format(time=time.time())

@app.route('/api/detect', methods=['POST'])
def detect():
    if request.method == 'POST':
        data = request.get_json()

        if data is None:
            print("No valid request body, json missing!")
        else:
            img_data = data['requests'][0]['image']['content']
            decoded_string = base64.b64decode(img_data)
            data = np.fromstring(decoded_string, dtype=np.uint8)

            #Start Detection
            color_image_flag = 1
            img = cv2.imdecode(data, color_image_flag)

            img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
            im = (cv2.resize(img, (32, 32)).astype(np.float32))/255.0
            global graph
            with graph.as_default():
                pr = model.predict(im.reshape((1,32,32,1)), verbose=0)
                pr_id = np.argmax(pr[0])
                pr_proba = pr[0,pr_id]
                print(pr_proba)

            print("Result : ",classout[pr_id])
            # build a response dict to send back to client
            response = {"responses": [
                            {
                              "id": str(pr_id),
                              "class": classout[pr_id],
                              "score": str(pr_proba),
                              "description": "-"
                            }
                          ]
                        }

            # encode response using jsonpickle
            response_pickled = jsonpickle.encode(response)
            return Response(response=response_pickled, status=200, mimetype="application/json")

# start flask app
# app.run(host="0.0.0.0", port=8080)
if __name__ == '__main__':
    app.run(debug=True)
